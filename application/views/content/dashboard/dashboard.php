        <div class="content">
            <div class="content-header">
                <div class="leftside-content-header">
                    <ul class="breadcrumbs">
                        <li><i class="fa fa-home" aria-hidden="true"></i><a href="<?=site_url()?>">Dashboard</a></li>
                    </ul>
                </div>
            </div>
            <div class="row animated fadeInUp">
                <div class="col-sm-12 col-md-12">
                    <h4 class="section-subtitle">Welcome <strong><?=$this->session->userdata['user_nickname']?></strong>, di e-Forecasting UCIC</h4>
                    <div class="panel">
                        <div class="panel-content">
                            <p class="text-justify">
                                Forecasting atau perkiraan adalah kegiatan yang bertujuan untuk meramalkan atau memprediksi segala hal yang terkait dengan produksi, penawaran, permintaan ,dan penggunaan teknologi dalam sebuah industri atau usaha. Perkiraan ini pada akhirnya akan digunakan oleh perusahaan maupun pihak manajemen operasional untuk membuat perencanaan terkait kegiatan usaha dalam beberapa periode tertentu.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>